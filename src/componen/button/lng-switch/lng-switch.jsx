import React, {useState} from "react";
import { useTranslation } from 'react-i18next';

import "./ling-switch.scss"

export function LanguageSwitcher() {
    const { i18n } = useTranslation();
  
    const [lngs] = useState({
      uk: { nativeName: 'uk' },
      en: { nativeName: 'en' },
    });
    const [lngSwitcherHover, setLngSwitcherHover] = useState(false);
  
    const filteredLngs = Object.values(lngs).filter(
      (lng) => lng.nativeName !== i18n.resolvedLanguage,
    );
  
    return (
      <div className="leng-body">
        <div className="leng-case ">
          <button className="nav-button">{i18n.resolvedLanguage}</button>
          {
            filteredLngs.map((lng) => (
              <button
                className="nav-button nav-button_next"
                key={lng.nativeName}
                onClick={() => i18n.changeLanguage(lng.nativeName)}
              >
                {lng.nativeName}
              </button>
            ))
          }
        </div>
      </div>
    );
  }
  