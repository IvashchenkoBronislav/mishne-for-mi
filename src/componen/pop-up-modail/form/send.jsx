import emailjs from "@emailjs/browser";
import React from "react"

const SERVICE_ID = "";
const TEMPLATE_ID = "";
const USER_ID = "";

emailjs.init(USER_ID);
export function sendEmail(form){

    emailjs.send(SERVICE_ID, TEMPLATE_ID, form, USER_ID)
    .then((result) => {
        console.log(result.text);
    }, (error) => {
        console.log(error.text);
    });
}